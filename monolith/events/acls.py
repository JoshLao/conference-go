from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests
import json

def get_photo(state):
    url = 'https"//api.pexels.com/v1/search?query=nature&per_page=1'
    headers = {"Authorization": PEXELS_API_KEY}
    response = requests.get(url,headers=headers)
    photo_data = json.loads(response.content)
    photo_url = {
        "photo_url": photo_data["photos"][0]["url"]
    }
    print(photo_url)
    return photo_url

def get_weather_data(city,state):
    # Create the URL for the geocoding API with the city and state
    geo = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},840&appid={OPEN_WEATHER_API_KEY}"
    # Make the request
    response = requests.get(geo)
    # Parse the JSON response
    geo_data = json.loads(response.content)
    print("Geo_data: ",geo_data[0]["lat"])
    # Get the latitude and longitude from the response
    latitude = geo_data[0]["lat"]
    longitude = geo_data[0]["lon"]
    # Create the URL for the current weather API with the latitude
    weather = f"https://api.openweathermap.org/data/2.5/weather?lat={latitude}&lon={longitude}&appid={OPEN_WEATHER_API_KEY}"
    #   and longitude
    # Make the request
    response = requests.get(weather)
    # Parse the JSON response
    weather_data = json.loads(response.content)
    # Get the main temperature and the weather's description and put
    print(weather_data["main"]["temp"])
    weather_dict = {
        "temp": (weather_data["main"]["temp"]-273.15)*(9/5)+32,
        "description": weather_data["weather"][0]["description"]
    }
    #   them in a dictionary
    # Return the dictionary
    return weather_dict
