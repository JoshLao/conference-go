from django.apps import AppConfig
from django.urls import reverse

class PresentationsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'presentations'

    def get_api_url(self):
        return reverse("api_list_presentations", kwargs={"id": self.id})